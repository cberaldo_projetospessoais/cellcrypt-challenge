# CellCrypt Challenge - App planning

## Application Requirements

- Execution must be under 2s.
- Create unit tests for the application.

## Challenge

Create a function `f(range)` that will:

- Receive the range that will be used to calculate.
- Calculate the factorial for the given range.
- Sum each digit of the factorial.
- Return the sum of all digits of the factorial.

## Testing

Create a test unit to validate the function:

- Decide which technology will be used.
- Add dependencies.
- Create testing units.

## Running the application

```bash

npm install

npm run start

```
