const BigInt = require('big-integer');

/**
 * This function will calculate the factorial sum of the values.
 * @param {number} sum the accumulator of the reducer function.
 * @param {number} number the current value of the reducer function.
 */
const calculateFactorial = (sum, number) => BigInt(sum).value * BigInt(number).value;

/**
 * This function will sum all digits of a given number.
 * @param {number} number the number to sum the digits.
 */
const sumDigits = (number) => {
  const arrayOfDigits = String(number).split('');
  const sum = arrayOfDigits.reduce((prev, curr) => Number(prev) + Number(curr));
  return Number(sum);
};

/**
 * This function will return the factorial for the given range (0-2000).
 * @param {number} range the fatorial range.
 */
const factorial = (range) => {
  if (!range || range === 0) { return 0; }

  if (range > 2000 || range < 0) { throw new Error('Exceeded input range (0, 2000).'); }

  // create an array for the given range.
  // increase the range value by 1 to ignore the first index of array (zero).
  let factorialArray = Array.from(Array((range + 1)).keys());

  // ignore the first element of the array (zero).
  factorialArray = factorialArray.slice(1);

  // reverse the array to calculate.
  factorialArray = factorialArray.reverse();

  // reduce the array to the factorial, starting in 1.
  const factorialSum = factorialArray.reduce(calculateFactorial, 1);

  // return the factorial as a big int value.
  return BigInt(factorialSum).value;
};

/**
 * This function will return then sum of all digits for the given range (0-2000).
 * @param {number} range the fatorial range.
 */
const sumDigitsFactorial = (range) => sumDigits(factorial(range));

module.exports = sumDigitsFactorial;
