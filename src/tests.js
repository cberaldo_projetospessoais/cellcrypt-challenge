const assert = require('assert');
const logger = require('consola');
const sumDigitsFactorial = require('./factorial');

/**
 * This function will test and compare the result of the factorial function.
 * @throws exception when test fails.
 * @param {number} input the input value for the factorial function.
 * @param {number} output then expected result of the sum of the digits.
 */
const assertEqual = (info, input, output) => {
  logger.info(`${info} ${input}`);
  assert.equal(output, sumDigitsFactorial(input));
  logger.success('Successful');
};

const assertDoesNotThrow = (info, input) => {
  logger.info(info);
  assert.doesNotThrow(() => sumDigitsFactorial(input));
  logger.success('Successful');
};

const assertThrows = (info, input) => {
  logger.info(info);
  assert.throws(() => sumDigitsFactorial(input));
  logger.success('Successful');
};

const runTests = () => {
  // validate invalid input.
  assertDoesNotThrow('doesNotThrow when input is undefined');
  assertDoesNotThrow('doesNotThrow when input is invalid', 'sadfsdasa');

  // validate exceeded max range.
  assertThrows('throws when input exceeds 2000', 2001);
  assertThrows('throws when input exceeds by far 2000', 51651684);
  assertThrows('throws when input is negative', -1);

  // validate outputs.
  assertEqual('succeded when input is', 0, 0);
  assertEqual('succeded when input is', 1, 1);
  assertEqual('succeded when input is', 2, 2);
  assertEqual('succeded when input is', 4, 6);
  assertEqual('succeded when input is', 10, 27);
  assertEqual('succeded when input is', 1000, 10539);
  assertEqual('succeded when input is', 2000, 23382);
};

module.exports = runTests;
