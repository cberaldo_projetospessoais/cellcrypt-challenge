const logger = require('consola');
const runTests = require('./tests');

const run = () => {
  try {
    runTests();

    logger.success('Execution completed.');
  } catch (error) {
    logger.error(error);
  }
};

run();
